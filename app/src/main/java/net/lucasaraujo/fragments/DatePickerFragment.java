package net.lucasaraujo.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;

import net.lucasaraujo.androidstudy.UI_ViewDatetimePickers;

import java.util.Calendar;

/**
 * Created by Lucas on 21/09/2014.
 */
public class DatePickerFragment extends android.support.v4.app.DialogFragment {
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final Calendar c = Calendar.getInstance();

        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        UI_ViewDatetimePickers context = (UI_ViewDatetimePickers) getActivity();
        return new DatePickerDialog(context, context, year, month, day);
    }
}
