package net.lucasaraujo.androidstudy;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import net.lucasaraujo.InterfaceData.DataItem;

import java.util.ArrayList;

/**
 * Created by Lucas on 18/09/2014.
 */
public class CustomAdapter extends BaseAdapter {
    private static LayoutInflater inflater = null;
    ArrayList<DataItem> data;
    Context context;

    public CustomAdapter(Context context, ArrayList<DataItem> data) {
        this.data = data;
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = inflater.inflate(R.layout.list_item, null);
        }
        TextView name = (TextView) view.findViewById(R.id.nameValue);
        TextView phone = (TextView) view.findViewById(R.id.phoneValue);

        DataItem d = data.get(i);

        name.setText(d.name);
        phone.setText(d.phone);

        return view;
    }
}
