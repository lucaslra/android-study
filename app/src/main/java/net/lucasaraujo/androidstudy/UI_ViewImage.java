package net.lucasaraujo.androidstudy;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;


public class UI_ViewImage extends Activity {

    Vibrator mgr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ui__view_image);
        mgr = (Vibrator) getSystemService(this.VIBRATOR_SERVICE);

        long[] arr = new long[]{1000, 1000, 2000, 2000, 3000, 3000, 4000, 4000};
        mgr.vibrate(1000);
    }

    private void ChangeImage()
            throws InterruptedException {
        Thread.sleep(4000);
        ImageView img = (ImageView) findViewById(R.id.myImage);

        Resources res = getResources();
        Drawable d = res.getDrawable(R.drawable.ic_wifi);
        img.setImageDrawable(d);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mgr.cancel();
    }

    public void doSomeStuff(View view) {
        final AlertDialog.Builder dlg = new AlertDialog.Builder(this);

        dlg.setTitle("Click on ImageView")
                .setIcon(android.R.drawable.ic_lock_lock)
                .setMessage("You clicked in an ImageView")
                .setNeutralButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dlg.setTitle("An alert dialog inside another alert dialog?")
                                .setIcon(android.R.drawable.ic_dialog_map)
                                .setMessage("Alertinception @_@")
                                .setNeutralButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        try {
                                            ChangeImage();
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }).show();
                    }
                }).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.ui__view_image, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
