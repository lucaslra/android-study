package net.lucasaraujo.androidstudy;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RadioGroup;
import android.widget.TextView;


public class UI_ViewRadioButton extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ui__view_radio_button);

        RadioGroup rg = (RadioGroup) findViewById(R.id.myRadioGroup);

        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                TextView t = (TextView) findViewById(R.id.myText);

                if (radioGroup.getCheckedRadioButtonId() == R.id.sans) {
                    t.setTypeface(Typeface.SANS_SERIF);
                } else if (radioGroup.getCheckedRadioButtonId() == R.id.serif) {
                    t.setTypeface(Typeface.SERIF);
                } else if (radioGroup.getCheckedRadioButtonId() == R.id.mono) {
                    t.setTypeface(Typeface.MONOSPACE);
                }
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.ui__view_radio_button, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
