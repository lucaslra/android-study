package net.lucasaraujo.androidstudy;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.Random;


public class UI_ViewCheckBox extends Activity {

    // Services
    Vibrator vbr;
    private boolean isBold = false;
    private boolean isCapitalized = false;
    private boolean isColored = false;
    private boolean isBig = false;
    private float originalSize;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ui__view_check_box);

        TextView t = (TextView) findViewById(R.id.changeText);
        originalSize = t.getTextSize();

        vbr = (Vibrator) getSystemService(this.VIBRATOR_SERVICE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.ui__view_check_box, menu);
        return true;
    }

    public void checkClicked(View view) {
        CheckBox c = (CheckBox) view;

        TextView t = (TextView) findViewById(R.id.changeText);
        vbr.vibrate(200);

        if (c.getId() == R.id.bold) {
            if (!this.isBold) {
                t.setTypeface(Typeface.DEFAULT_BOLD);
                isBold = true;
            } else {
                t.setTypeface(Typeface.DEFAULT);
                isBold = false;
            }
        } else if (c.getId() == R.id.capitalize) {
            t.setAllCaps(!isCapitalized);
            isCapitalized = !isCapitalized;
        } else if (c.getId() == R.id.color) {
            if (!this.isColored) {
                t.setTextColor(new Random(new Random().nextInt()).nextInt());
                this.isColored = true;
            } else {
                t.setTextColor(Color.BLACK);
                this.isColored = false;
            }
        } else if (c.getId() == R.id.bigfy) {
            if (!this.isBig) {
                t.setTextSize(50);
                this.isBig = true;
            } else {
                t.setTextSize(originalSize);
                this.isBig = false;
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
