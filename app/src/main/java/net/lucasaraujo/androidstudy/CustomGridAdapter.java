package net.lucasaraujo.androidstudy;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import net.lucasaraujo.InterfaceData.GridDataItem;

import java.util.ArrayList;

/**
 * Created by Lucas on 18/09/2014.
 */
public class CustomGridAdapter extends BaseAdapter {
    private static LayoutInflater inflater = null;
    ArrayList<GridDataItem> data;
    Context context;

    public CustomGridAdapter(Context context, ArrayList<GridDataItem> data) {
        this.data = data;
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = inflater.inflate(R.layout.grid_item, null);
        }
        ImageView image = (ImageView) view.findViewById(R.id.image);
        TextView imageText = (TextView) view.findViewById(R.id.imageText);

        GridDataItem d = data.get(i);

        image.setImageResource(d.image);
        imageText.setText(d.text);

        return view;
    }
}
