package net.lucasaraujo.androidstudy;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;


public class UI_ViewSpinner extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ui__view_spinner);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.colors_spin, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Spinner spin = (Spinner) findViewById(R.id.mySpinner);
        spin.setAdapter(adapter);

        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String sel = (String) adapterView.getItemAtPosition(i);
                TextView t = (TextView) findViewById(R.id.myText);
                setTextColor(sel);
            }

            private void setTextColor(String color) {
                String hexColor = "#FF000000";
                if (color.equals("Red")) {
                    hexColor = "#FFAA0000";
                } else if (color.equals("Orange")) {
                    hexColor = "#FFCC6600";
                } else if (color.equals("Yellow")) {
                    hexColor = "#FFCCAA00";
                } else if (color.equals("Green")) {
                    hexColor = "#FF00AA00";
                } else if (color.equals("Blue")) {
                    hexColor = "#FF0000AA";
                } else if (color.equals("Violet")) {
                    hexColor = "#FF6600AA";
                } else if (color.equals("Black")) {
                    hexColor = "#000000";
                }
                TextView text = (TextView) findViewById(R.id.myText);
                text.setTextColor(Color.parseColor(hexColor));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.ui__view_spinner, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
