package net.lucasaraujo.androidstudy;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;

import net.lucasaraujo.InterfaceData.DataItem;
import net.lucasaraujo.InterfaceData.GridDataItem;

import java.util.ArrayList;


public class CustomGridLayoutActivity extends Activity {

    ArrayList<DataItem> data;
    CustomAdapter adapter;
    ArrayList<GridDataItem> data2;
    CustomGridAdapter adapter2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_grid_layout);

        data = new ArrayList<DataItem>();
        data.add(new DataItem("Lucas Araujo", "991944889"));
        data.add(new DataItem("Juliane Macedo", "093019230"));
        data.add(new DataItem("Nirvana Mendes", "920910230"));

        data2 = new ArrayList<GridDataItem>();
        data2.add(new GridDataItem(R.drawable.ic_alert, "Alerta"));
        data2.add(new GridDataItem(R.drawable.ic_error, "Error"));
        data2.add(new GridDataItem(R.drawable.ic_android, "Android"));
        data2.add(new GridDataItem(R.drawable.ic_star, "Star"));
        data2.add(new GridDataItem(R.drawable.ic_wifi, "Wi-fi"));

        adapter = new CustomAdapter(this, data);
        adapter2 = new CustomGridAdapter(this, data2);

        ListView myList = (ListView) findViewById(R.id.listView);
        myList.setAdapter(adapter);

        GridView myGrid = (GridView) findViewById(R.id.gridView);
        myGrid.setAdapter(adapter2);

        final AlertDialog.Builder alt = new AlertDialog.Builder(this);

        myList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                TextView t = (TextView) view.findViewById(R.id.phoneValue);

                alt.setTitle("Phone Number")
                        .setMessage(t.getText())
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue with delete
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .show();
            }
        });

        myList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                TextView t = (TextView) view.findViewById(R.id.nameValue);

                alt.setTitle("Nome")
                        .setMessage(t.getText())
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue with delete
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .show();

                return true;
            }
        });

        myGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                TextView t = (TextView) view.findViewById(R.id.imageText);

                alt.setTitle("Imagem")
                        .setMessage(t.getText())
                        .setPositiveButton(android.R.string.copy, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue with delete
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .show();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.custom_grid_layout, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
