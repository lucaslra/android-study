package net.lucasaraujo.androidstudy;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;

import net.lucasaraujo.fragments.DatePickerFragment;

public class UI_ViewDatetimePickers extends FragmentActivity
        implements DatePickerDialog.OnDateSetListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ui__view_datetime_pickers);
    }

    public void getDate(View view) {
        DialogFragment picker = new DatePickerFragment();

        picker.show(getSupportFragmentManager(), "datepicker");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.ui__view_datetime_pickers, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDateSet(DatePicker datePicker, int i, int i2, int i3) {
        TextView t = (TextView) findViewById(R.id.dateText);
        String message = String.format("Selected date: %d/%d/%d", i, i2 + 1, i3);

        t.setText(message);
    }
}
