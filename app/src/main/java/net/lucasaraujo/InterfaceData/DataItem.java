package net.lucasaraujo.InterfaceData;

/**
 * Created by Lucas on 18/09/2014.
 */
public class DataItem {
    public String name;
    public String phone;

    public DataItem(String name, String phone) {
        this.name = name;
        this.phone = phone;
    }
}
