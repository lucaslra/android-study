package net.lucasaraujo.InterfaceData;

/**
 * Created by Lucas on 18/09/2014.
 */
public class GridDataItem {
    public int image;
    public String text;

    public GridDataItem(int image, String text) {
        this.image = image;
        this.text = text;
    }
}
